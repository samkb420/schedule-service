require("dotenv").config();
require("../config/db").connect();
const nodemailer = require("nodemailer");
const cron = require("node-cron");

const AfricasTalking = require("africastalking"); 

const Schedule = require("../models/Schedule");

const africastalking = AfricasTalking({
  apiKey: "e95f2828066666bdfa2713558b7abc8137cbbb73f7dcf3e62236cd374cfdcc80",
  username: "sandbox",
});





exports.getSchedules = async (req, res, next) => {
  try {
    const schedule = await Schedule.find();

    return res.status(200).json({
      success: true,
      count: schedule.length,
      data: schedule,
    });
  } catch (err) {
    return res.status(500).json({
      success: false,
      error: "Server Error",
    });
  }
};


exports.addSchedule = async (req, res, next) => {
  try {
    const { title, description, date ,notification } = req.body;

    const schedule = await Schedule.create(req.body);


    const message = ```
    Hey There,
    
    A Schedule with the title  ${title} and ${description},
    have been added and its due on ${date}


    -- From SmartHome Solution 
   
    ```;

  

    
    const transporter = nodemailer.createTransport({
        host: "mail.kenyaweb.com",
        port: 587,
        secure: false, // true for 465, false for other ports
        auth: {
          user: "notifications@kenyaweb.com", // generated ethereal user
          pass: "@NOTEooo", // generated ethereal password
        },
      });

      const mailOptions = {
        from: "notifications@kenyaweb.com",
        to: "samuel.kago@kenyaweb.com",
        bcc: "kago.samuel.mburu@gmail.com",
        subject: "notification",
        text: message,
      };

       transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
          console.log(error);
        } else {
          console.log("Email sent: " + info.response);

          return res.status(201).json({
      success: true,
      data: schedule,
    });
        }
      });

    
   


  
    
    
  } catch (err) {
    if (err.name === "ValidationError") {
      const messages = Object.values(err.errors).map((val) => val.message);

      return res.status(400).json({
        success: false,
        error: messages,
      });
    } else {
      return res.status(500).json({
        success: false,
        error: "Server Error",
      });
    }
  }
};



exports.deleteSchedule = async (req, res, next) => {
  try {
    const schedule = await Schedule.findById(req.params.id);

    if (!schedule) {
      return res.status(404).json({
        success: false,
        error: "No task found",
      });
    }

    await schedule.remove();

    return res.status(200).json({
      success: true,
      data: {},
    });
  } catch (err) {
    return res.status(500).json({
      success: false,
      error: "Server Error",
    });
  }
};

exports.updateSchedule = async (req, res, next) => {
  try {
    const { title, description, date,notification } = req.body;

    let schedule = await Schedule.findById(req.params.id);

    if (!schedule) {
      return res.status(404).json({
        success: false,
        error: "No task found",
      });
    }

    schedule = await Schedule.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
      runValidators: true,
    });

    return res.status(200).json({
      success: true,
      data: task,
    });
  } catch (err) {
    if (err.name === "ValidationError") {
      const messages = Object.values(err.errors).map((val) => val.message);

      return res.status(400).json({
        success: false,
        error: messages,
      });
    } else {
      return res.status(500).json({
        success: false,
        error: "Server Error",
      });
    }
  }
};



exports.getSchedule = async (req, res, next) => {
  try {
    const schedule = await Schedule.findById(req.params.id);

    if (!schedule) {
      return res.status(404).json({
        success: false,
        error: "No task found",
      });
    }

    return res.status(200).json({
      success: true,
      data: schedule,
    });
  } catch (err) {
    return res.status(500).json({
      success: false,
      error: "Server Error",
    });
  }
};



