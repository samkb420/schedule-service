// Task model
const mongoose = require('mongoose');

const ScheduleSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true,
        trim: true
    },
    description: {
        type: String,
        required: true,
        trim: true
    },
    date: {
        type: Date,
        required: true,
        trim: true
    },
    notification: {
        type: Boolean,
        default:true,
        trim: true
    }


});


module.exports = mongoose.model('Schedule', ScheduleSchema);

