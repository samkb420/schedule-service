// router




const express = require("express");
const {getSchedules,addSchedule,deleteSchedule,updateSchedule,getSchedule } = require("../controllers/ScheduleController");

const router = express.Router();




  





router.get("/schedules", getSchedules);

router.post("/schedules", addSchedule);



router.delete("/schedules/:id", deleteSchedule);


router.put("/schedules/:id", updateSchedule);



router.get("/schedules/:id", getSchedule);

module.exports = {
    routes:router
}


